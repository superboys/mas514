.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. image:: ./figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

.. image:: ./figs/jetbot.jpg
   :width: 12cm
   :align: center

|
|

MAS514 - Robotics and Instrumentation
===================================================

Lecturer
---------

- Daniel Hagen, PhD
   - Associate Professor
   - https://www.uia.no/en/kk/profile/danielh

Authors of this Sphinx report
---------

- Lars Løvtangen Borgen
   - Mechatronics master student


- Joakim Dahl Wold
   - Mechatronics master student


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   src/Getting started
   src/Navigation Stack
   src/Remote communication - SSH
   src/Odometry and Inverse Kinematics.rst
   src/Teleoperation
   
   <!--src/start-->
   <!--src/report-->
   <!--src/ros-->
   <!--src/web-controller-->
   <!--src/troubleshooting-->
   <!--src/calibration-->
   <!--src/strawberry-->


.. Indices and tables
.. ==================



.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
