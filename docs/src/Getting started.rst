Getting started
===============

This guide assumes that the reader has followed the guides found at https://hagenmek.gitlab.io/mas514, as it provides the neccesary files to get started in addition to gaining some prelimenary knowledge about the use of a Jetbot.


Equipment
----------------
To be able to complete the project, you will need the following equipment:

 - Jetbot, details can be found at https://jetbot.org/master/
 - LIDAR L515
 - 3D printer (if needed to mount the LIDAR)
 

Continue our work
----------------
This guide will show you how to get certain functions up and running on your own system, but if you just want to copy/clone our work so far, you can clone the repository found here: https://gitlab.com/superboys/mas514/ using the following command:
 - git clone https://gitlab.com/superboys/mas514.git
