Odometry and Inverse Kinematics of the Jetbot
================



.. image:: ../figs/overview_tf.png
   :width: 600px
   :align: center
   :alt: alternate text


1. Odometry
----------------
The odometry node was copied from https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-publish-odometry-using-twist and uses twist to publish the odometry of the robot. 

2. Inverse Kinematics
----------------
Inverse kinematics was also controlled by twist and the code was found here: https://hagenmek.gitlab.io/mas514/src/guides.html#how-to-subscribing-twist-command. However, using this script without changes caused the jetbot to not move until a very high velocity command came in. The incoming commands were multiplied by 10 to make the odometry and the actual movement of the robot coincide better. Further tuning of this parameters could be done.






