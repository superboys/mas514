Tele-operating of Jetbot
================



.. image:: ../figs/overview_tf.png
   :width: 600px
   :align: center
   :alt: alternate text


1. A twist teleoperating node was cloned to the work space from https://github.com/ros-teleop/teleop_twist_keyboard. 
This publishes a "cmd_vel" topic which can be subscribed to both to control the jetbot and to estimate the odometry of the robot. The speed was adjusted down as it made the jetbot fly off the grid in rviz.

----------------

2. Launch

To run the node, the following can be typed in the terminal: rosrun teleop_twist_keyboard teleop_twist_keyboard.py"

----------------

