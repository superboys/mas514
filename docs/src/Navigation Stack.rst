Navigation Stack
================
This page takes you through the steps needed to be able to setup navigation stack on the Jetbot.


.. image:: ../figs/overview_tf.png
   :width: 600px
   :align: center
   :alt: alternate text


1. Create package
----------------

First we need to create a package for storing all the configuration and launch files used by the navigation stack. Choose where you want to create the package, and run the command below to create the package for navigation stack including dependencies needed.

    * ``"catkin_create_pkg my_robot_name_2dnav move_base my_tf_configuration_dep my_odom_configuration_dep my_sensor_configuration_dep"``


2. Creating the robot configuration launch file
----------------

With the workspace created, and we have somewhere to work, we can create a roslaunch file that brings up the hardware and transform publishers that the robot requires. Copy and paste the launch parameters below into a new file you have to create called my_robot_configuration.launch. Replace "my_robot" with the chosen name you have for the robot. 

<launch>

   <node pkg="sensor_node_pkg" type="sensor_node_type" name="sensor_node_name" output="screen">
    <param name="sensor_param" value="param_value" />
 </node>
 <node pkg="odom_node_pkg" type="odom_node_type" name="odom_node" output="screen">
    <param name="odom_param" value="param_value" />
 </node>
 <node pkg="transform_configuration_pkg" type="transform_configuration_type" name="transform_configuration_name" output="screen">
    <param name="transform_configuration_param" value="param_value" />
 </node>

</launch> 


Building upon this launch file template, some changes will have to be made, as shown in sections below.


<launch>

   <node pkg="sensor_node_pkg" type="sensor_node_type" name="sensor_node_name" output="screen">


Now bring up the sensor(s) for the robot to use. You will have to replace "sensor_node_pkg" with the name of your ROS driver package name for the chosen sensor, "sensor_node_type" should be the type of driver, "sensor_node_name" should be the name you want for your sensor node, and at last the "sensor_param" should be updated with parameters for your node. 

</node>
 <node pkg="odom_node_pkg" type="odom_node_type" name="odom_node" output="screen">
    <param name="odom_param" value="param_value" />
 </node>


Now to launch the odometry for the base, replace "odom_xxxxx" with the package, type, name and parameters relevant to the node that you are launching.

 <param name="transform_configuration_param" value="param_value" />
 </node>


Now to launch the transform configuration of the robot. As before replace "transform_xxxxxxxx" with the correct pkg, type, name, parameters.

3. Configuration of the Costmap (local & global)
----------------

- Global costmap
- Local costmap
- Configuration options


4. Configuration of the Base local planner
----------------



5. Creating the navigation stack launch file
----------------



6. AMCL - Optimization (optional)
----------------
If you would like to optimize the system, have a look at http://wiki.ros.org/amcl 



