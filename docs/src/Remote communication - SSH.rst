Remote communication using SSH protocol
================
With the help of this guide, you will be able to connect wirelessly to the Jetbot, using Secure Shell via Ubuntu 18.04. 
Ubuntu can be installed either as the main OS for your system, or use virtual computer software to install Ubuntu in a closed environment on your computer. 

This guide will go through the steps for installing it through VirtualBox. 

1. Install VirtualBox 
----------------
Go to https://www.virtualbox.org/wiki/Downloads and choose your operating system. The .exe file for installing Virtualbox will start to download.  

Run the file, choose where you want to install it and click next until it starts installing. Wait a bit and you are done.  


2. Install ubuntu 
----------------
Go to https://releases.ubuntu.com/18.04/ to download Ubuntu(Bionic Beaver). It is the ISO image file that you need.
Open VirtualBox and click on "New"(blue icon).  

Make sure Type is set to Linux and version is Ubuntu 64-bit. 

->Next 

Then you need to assign the virtual system some RAM, recommend at least 2GB of RAM, and 4GB if your main system can afford it. 

-> Next

Create virtual hard disk now

-> Next 

VDI(VirtualBox Disk Image)  

-> Next  

Choose one of the two options regarding storage, this is your own choice but Fixed size often results in a faster system.   

-> Next  

About 10GB should be fine, although if later there is a need to increase the size, it is possible to do so.  

-> Create  

Now you should be able to select the newly created virtual machine and click the green arrow at the top (Start).

-> Start

A pop-up should appear where you may need to click the folder icon and choose your downloaded ISO file manually, if VirtualBox cannot find it automatically.

-> Start

Choose install Ubuntu

-> Install

You may deselect "Download updates while installing Ubuntu" to speedup the installation process.

-> Continue

Select "Erase disk and install Ubuntu", this will ONLY affect your virtual computer, not your host machine.

-> Install now

-> Continue

The rest of the steps are for your personal experience, and there is no "one solution fits all" here. 
After the last steps are completed where you chose country, keyboard, username, password, etc.. The system wants to restart. Do that.
Eventually a screen will appear with a message to remove the installation medium and press ENTER.

-> Press ENTER

You are now done installing Ubuntu 18.04 on a Virtual machine.

3. SSH
----------------

Now to connect to your robot via SSH, you first need to enable SSH on your newly installed ubuntu system.
Start with installing openssh server via these commands: 
 sudo apt update 
 sudo apt install openssh-server 

Check if the ssh server is running correctly. Write this in the terminal and check if there is green highlited text saying Active(running).
 sudo systemctl status ssh 

If the built in firewall in Ubuntu is enabled, use this command to open the port for SSH.
 sudo ufw allow ssh 

Now you are ready for connecting to your Jetbot.

In terminal write the following command, where the ip xxx.xxx.xxx.xxx should be replaced by the ip of your Jetbot connected to the same internet as your Ubuntu remote system. If you need to find the ip of your Jetbot, you can use the command "ifconfig" in the terminal of your Jetbot.

 ssh usernameofJetbotsystem@xxx.xxx.xxx.xxx 

Congratulations, you can now use your Ubuntu system to control the Jetbot remotely.







