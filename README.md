# MAS514 - Robotics and Instrumentation
This repo contains code and guides for others to use to get started within the field of autonomous control of a jetbot robot vehicle. The information and guide builds upon the course MAS514 teached at the University of Agder in Norway.
The sphinx guide along with documentation, and code can be found at https://superboys.gitlab.io/mas514/


| Year | Authors                  | 
| ---- | :-------------------------- | 
| 2021 | Joakim Dahl Wold, Lars Løvtangen Borgen     | 

| Year | Lecturer                    | 
| ---- | :-------------------------- | 
| 2021 | Daniel Hagen         | 


